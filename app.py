from flask import Flask                                                        
from twilio import twiml                                                       

try:
    # Free accounts have limited whitelist of sites through proxy
    proxy = os.environ['http_proxy']
    if proxy:
        host, port = urlparse(os.environ["http_proxy"]).netloc.split(":")
        Connection.set_proxy_info(
                host,
                int(port),
                proxy_type=PROXY_TYPE_HTTP,
            )
except: 
    # Paid accounts don't have/need the proxy
    pass

app = Flask(__name__)

@app.route('/conference', methods=['POST'])                                    
def voice():
    response = twiml.Response()  
 
    with response.dial() as dial:                                          
        dial.conference("Party Line for Party People")                                    
 
    return str(response)
 
if __name__ == "__main__":
    app.debug = True
    app.run(port=5000)

