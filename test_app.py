import unittest
import pytest

from app import app

from xml.etree import ElementTree


class TwiMLTest(unittest.TestCase):
    def setUp(self):
        self.test_app = app.test_client()

    def assertTwiML(self, response):
        assert response.status == "200 OK"
        root = ElementTree.fromstring(response.data)
        assert root.tag == 'Response', "Did not find tag as root element " \
                "TwiML response."

    def call(self, url='/voice', to='+15550001111',
            from_='+15558675309', digits=None, extra_params=None):
        """Simulates Twilio Voice request to Flask test client
 
        Keyword Args:
            url: The webhook endpoint you wish to test. (default '/voice')
            to: The phone number being called. (default '+15550001111')
            from_: The CallerID of the caller. (default '+15558675309')
            digits: DTMF input you wish to test (default None)
            extra_params: Dictionary of additional Twilio parameters you
                wish to simulate, like QueuePosition or Digits. (default: {})
 
        Returns:
            Flask test client response object.
        """
 
        # Set some common parameters for messages received by Twilio.
        params = {
            'CallSid': 'CAtesting',
            'AccountSid': 'ACxxxxxxxxxxxxx',
            'To': to,
            'From': from_,
            'CallStatus': 'ringing',
            'Direction': 'inbound',
            'FromCity': 'BROOKLYN',
            'FromState': 'NY',
            'FromCountry': 'US',
            'FromZip': '55555'}
 
        # Add simulated DTMF input.
        if digits:
            params['Digits'] = digits
 
        # Add extra params not defined by default.
        if extra_params:
            params = dict(params.items() + extra_params.items())
 
        # Return the app's response.
        return self.test_app.post(url, data=params)
 
    def message(self, body, url='/message', to="+15550001111",
            from_='+15558675309', extra_params={}):
        """Simulates Twilio Message request to Flask test client
 
        Args:
            body: The contents of the message received by Twilio.
 
        Keyword Args:
            url: The webhook endpoint you wish to test. (default '/sms')
            to: The phone number being called. (default '+15550001111')
            from_: The CallerID of the caller. (default '+15558675309')
            extra_params: Dictionary of additional Twilio parameters you
                wish to simulate, like MediaUrls. (default: {})
 
        Returns:
            Flask test client response object.
        """
 
        # Set some common parameters for messages received by Twilio.
        params = {
            'MessageSid': 'SMtesting',
            'AccountSid': 'ACxxxxxxx',
            'To': to,
            'From': from_,
            'Body': body,
            'NumMedia': 0,
            'FromCity': 'BROOKLYN',
            'FromState': 'NY',
            'FromCountry': 'US',
            'FromZip': '55555'}
 
        # Add extra params not defined by default.
        if extra_params:
            params = dict(params.items() + extra_params.items())
 
        # Return the app's response.
        return self.test_app.post(url, data=params)



class TestConference(TwiMLTest):
    def test_conference(self):
        response = self.call(url='/conference')
        self.assertTwiML(response)

    def test_conference_valid(self):
        response = self.call(url='/conference')
 
        root = ElementTree.fromstring(response.data)
 
        dial_query = root.findall('Dial')
        assert len(dial_query) == 1, "Did not find one Dial verb, " \
                "instead found: %i " % len(dial_query)
 
        dial_children = list(dial_query[0])
        assert len(dial_children) == 1, "Dial does not go to one noun, " \
                "instead found: %s" % len(dial_children)
 
        assert dial_children[0].tag == 'Conference', "Dial is not to a Conference, " \
                "instead found: %s" % dial_children[0].tag
 
        assert dial_children[0].text == "Party Line for Party People", "Conference " \
                "is not Party Line for Party People, " \
                "instead found: %s" % dial_children[0].text

