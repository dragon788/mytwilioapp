# Personal Receptionist via Twilio
## Avoiding calls for fun and profit

This app was inspired by a talk at Prairie.Code() given by a consultant about how he handles recruiter calls.
Essentially he uses Twilio plus a hosted OpenVBX server to set up an IVR that screens the calls and allowsi
them to leave a voicemail or listen to his resume and availability or if he's currently looking for new work,
allows the recruiters to dial through to his cell (without revealing his number to them).

This application is designed to do the same thing, but rather than OpenVBX it uses a Flask app that can be run for free on PythonAnywhere.com to handle the IVR logic.

The IVR tree resembles something similar to the list below.

Configured Prompts:
* Please press 1 to leave a voicemail
* Please press 2 to hear my conditions of employment
* Please press 3 to hear my availability
* Please press 4 to hear my resume
* Please press 5 to dial my direct line

